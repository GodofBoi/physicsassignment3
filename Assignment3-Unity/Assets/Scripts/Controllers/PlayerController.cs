﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Your solution must make use of the following fields. If these values are changed, even at runtime,
    /// the character controller should respect the new values and act as detailed in the Unity inspector.
    /// </summary>

    [SerializeField]
    private float m_jumpApexHeight;

    [SerializeField]
    private float m_jumpApexTime;

    [SerializeField]
    private float m_terminalVelocity;

    [SerializeField]
    private float m_coyoteTime;

    [SerializeField]
    private float m_jumpBufferTime;

    [SerializeField]
    private float m_accelerationTimeFromRest;

    [SerializeField]
    private float m_decelerationTimeToRest;

    [SerializeField]
    private float m_accelerationTimeFromQuickturn;

    [SerializeField]
    private float m_decelerationTimeFromQuickturn;

    //My variables
    private Rigidbody2D shovelKnight;
    public float maxSpeedX;
    public float speedX;

    public enum FacingDirection { Left, Right }


    private void Start()
    {
        //Get the rigidbody component
        shovelKnight = GetComponent<Rigidbody2D>();
    }

    public bool IsWalking()
    {
        //If the player presses the right arrow key
        if(ShovelKnightInput.GetDirectionalInput(1).x > 0)
        {
            //Solve for acceleration
            /// Acceleration from rest is equal to velocity divided by time
            m_accelerationTimeFromRest = 16 / 7;

            speedX = speedX + maxSpeedX * (Time.deltaTime / m_accelerationTimeFromRest);

            //Limits the speed
            speedX = Mathf.Clamp(speedX, 0, maxSpeedX);

            //Adds speed to velocity x of the rigidbody
            shovelKnight.velocity = new Vector2(speedX, 0);

            return true;
        }

        //If the player presses the left arrow key
        if (ShovelKnightInput.GetDirectionalInput(1).x < 0)
        {
            //Solve for acceleration
            /// Acceleration from rest is equal to velocity divided by time
            m_accelerationTimeFromRest = 16 / 7;

            speedX = speedX + maxSpeedX * (Time.deltaTime / m_accelerationTimeFromRest);

            //Limits the speed
            speedX = Mathf.Clamp(speedX, 0, maxSpeedX);

            //Adds speed to velocity x of the rigidbody
            shovelKnight.velocity = new Vector2(-speedX, 0);

            return true;
        }
        else

            //Bring the speed of the player back to zero
            //Solve for Deacceleration
            m_decelerationTimeToRest = 16 / 4;

            speedX = speedX + maxSpeedX * (Time.deltaTime / -m_decelerationTimeToRest);
            shovelKnight.velocity = new Vector2(speedX, 0);

            return false;

        //throw new System.NotImplementedException( "IsWalking in PlayerController has not been implemented." );
    }

    public bool IsGrounded()
    {
        if (ShovelKnightInput.WasJumpPressed() == true)
        {


            return false;
        }

        if (ShovelKnightInput.IsJumpPressed() == true)
        {


            return false;
        }
        else

            shovelKnight.gravityScale = 7;
        return true;
        //throw new System.NotImplementedException( "IsGrounded in PlayerController has not been implemented." );
    }

    public FacingDirection GetFacingDirection()
    {

        
        throw new System.NotImplementedException( "GetFacingDirection in PlayerController has not been implemented." );
    }

    // Add additional methods such as Start, Update, FixedUpdate, or whatever else you think is necessary, below.
}
