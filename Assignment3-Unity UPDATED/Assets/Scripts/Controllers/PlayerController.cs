﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Your solution must make use of the following fields. If these values are changed, even at runtime,
    /// the character controller should respect the new values and act as detailed in the Unity inspector.
    /// </summary>

    [SerializeField]
    private float m_jumpApexHeight;

    [SerializeField]
    private float m_jumpApexTime;

    [SerializeField]
    private float m_terminalVelocity;

    [SerializeField]
    private float m_coyoteTime;

    [SerializeField]
    private float m_jumpBufferTime;

    [SerializeField]
    private float m_accelerationTimeFromRest;

    [SerializeField]
    private float m_decelerationTimeToRest;

    [SerializeField]
    private float m_maxHorizontalSpeed;

    [SerializeField]
    private float m_accelerationTimeFromQuickturn;

    [SerializeField]
    private float m_decelerationTimeFromQuickturn;

    public enum FacingDirection { Left, Right }




    // variables
    private bool DebugisGrounded, DebugisWalking;

    private bool isJumping;

    private Rigidbody2D shovelKnight;

    private int facingDirection;

    private float horizontalVelocity;

    private float playerGravity;

    private float initialVerticalVelocity;



    // initialize everything
    private void Awake()
    {
        shovelKnight = GetComponent<Rigidbody2D>();

        shovelKnight.constraints = RigidbodyConstraints2D.FreezeRotation;

        playerGravity = 2 * m_jumpApexHeight / Mathf.Pow(m_jumpApexTime, 2f);

        initialVerticalVelocity = 2 * m_jumpApexHeight / m_jumpApexTime;

        if(IsGrounded() != true)
        {
            isJumping = true;
        }
    }



    // update everything
    private void Update()
    {
        DebugisGrounded = IsGrounded();

        DebugisWalking = IsWalking();

        HorizontalMovement();

        Jump();
    }



    // check if is walking or not
    public bool IsWalking()
    {
        if (Mathf.Abs(ShovelKnightInput.GetDirectionalInput().x) > 0)
        { return true; }

        else
        {
            return false;
        }
    }



    // check if is grounded or not
    public bool IsGrounded()
    {
        Collider2D ground = Physics2D.BoxCast(transform.position, new Vector2(.4f, .57f), 0f, transform.up, 0f, LayerMask.GetMask("Ground")).collider;
        if (ground != null)
        {
            float yOffset = .53f;

            transform.position = new Vector2 (shovelKnight.position.x, Mathf.Clamp(shovelKnight.position.y, ground.transform.position.y + yOffset, 999));

            return true;
        }

        else
        {
            return false;
        }
    }



    // make variable for each direction so when checking and setting direction knight stays in that postion
    public FacingDirection GetFacingDirection()
    {
        if (ShovelKnightInput.GetDirectionalInput().x > 0)
        {
            facingDirection = 1;
        }
        if (ShovelKnightInput.GetDirectionalInput().x < 0)
        {
            facingDirection = -1;
        }

        if (facingDirection > 0)
        {
            return FacingDirection.Right;
        }
        else
        {
            return FacingDirection.Left;
        }

    }



    // horizontal movement
    void HorizontalMovement()
    {
        //make horizontal acceleration based on input
        if (IsWalking())
        {

            if (ShovelKnightInput.GetDirectionalInput().x > 0)
            {
                horizontalVelocity = Mathf.Clamp(horizontalVelocity + (m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.deltaTime), -m_maxHorizontalSpeed, m_maxHorizontalSpeed);
            }

            else if (ShovelKnightInput.GetDirectionalInput().x < 0)
            {
                horizontalVelocity = Mathf.Clamp(horizontalVelocity - (m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.deltaTime), -m_maxHorizontalSpeed, m_maxHorizontalSpeed);
            }

            shovelKnight.velocity = new Vector2(horizontalVelocity, shovelKnight.velocity.y);

        }



        //make horizontal decceleration based on input
        else
        {
            if (Mathf.Abs(shovelKnight.velocity.x) > 0.5)
            {

                if (shovelKnight.velocity.x > 0)
                {
                    horizontalVelocity -= m_maxHorizontalSpeed / m_decelerationTimeToRest * Time.deltaTime;
                }

                else if (shovelKnight.velocity.x < 0)
                {
                    horizontalVelocity += m_maxHorizontalSpeed / m_decelerationTimeToRest * Time.deltaTime;
                }
            }



            //else stop moving altogetehr
            else
            {
                horizontalVelocity = 0;
            }

            shovelKnight.velocity = new Vector2(horizontalVelocity, shovelKnight.velocity.y);
        }
    }



    // make it jump
    void Jump()
    {
        //check if it's grounded to jump, then jump
        if (IsGrounded())
        {
            if (ShovelKnightInput.WasJumpPressed())
            {
                isJumping = true;
                shovelKnight.velocity = new Vector2(shovelKnight.velocity.x, initialVerticalVelocity);
            }
        }


        //else push gravity down on knight
        else
        {
            if (isJumping)
            {
                shovelKnight.velocity = new Vector2(shovelKnight.velocity.x, Mathf.Clamp(shovelKnight.velocity.y - playerGravity * Time.deltaTime, -m_terminalVelocity, 999));

            //if it's grounded don't jump
                if(IsGrounded())
                {
                    isJumping = false;
                }
            }
        }


   
    }
}
